/*
 * #%L
 * JMexico :: Swing Editor Demo
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor.demo;

import fr.reseaumexico.model.InputDesign;
import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;

/** @author sletellier - letellier@codelutin.com */
public class MexicoEditorDemoUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_OPTION_CHANGED = "optionChanged";

    public static final String PROPERTY_INPUT_DESIGN = "inputDesign";

    public static final String PROPERTY_INPUT_DESIGN_FILE = "inputDesignFile";

    protected InputDesign inputDesign;

    protected File inputDesignFile;

    protected boolean optionChanged;

    public File getInputDesignFile() {
        return inputDesignFile;
    }

    public void setInputDesignFile(File inputDesignFile) {
        File oldInputDesignFile = getInputDesignFile();
        this.inputDesignFile = inputDesignFile;
        firePropertyChange(PROPERTY_INPUT_DESIGN_FILE, oldInputDesignFile, inputDesignFile);
    }

    public InputDesign getInputDesign() {
        return inputDesign;
    }

    public void setInputDesign(InputDesign inputDesign) {
        InputDesign oldInputDesign = getInputDesign();
        this.inputDesign = inputDesign;
        setOptionChanged(true);
        firePropertyChange(PROPERTY_INPUT_DESIGN, oldInputDesign, inputDesign);
    }

    public boolean isOptionChanged() {
        return optionChanged;
    }

    public void setOptionChanged(boolean optionChanged) {
        boolean oldValue = isOptionChanged();
        this.optionChanged = optionChanged;
        firePropertyChange(PROPERTY_OPTION_CHANGED, oldValue, optionChanged);
    }

}
