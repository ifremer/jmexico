/*
 * #%L
 * JMexico :: Swing Editor Demo
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor.demo;

import fr.reseaumexico.editor.factorValue.FactorValueEditorFactory;
import jaxx.runtime.context.JAXXInitialContext;
import jaxx.runtime.swing.ErrorDialogUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import java.io.File;

/**
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class RunMexicoEditorDemo {

    /** Logger */
    private static Log log = LogFactory.getLog(RunMexicoEditorDemo.class);

    public static void main(String... args) {

        try {

            I18n.init(new DefaultI18nInitializer("jmexico-editor-demo-i18n"), null);

            JAXXInitialContext context = new JAXXInitialContext();
            context.add(new FactorValueEditorFactory());
            MexicoEditorDemoUI mainUI = new MexicoEditorDemoUI(context);

            mainUI.init();

            if (args.length == 1) {

                // load input model
                File file = new File(args[0]);

                mainUI.getHandler().loadInputFile(file);
            }
        } catch (Exception eee) {
            log.error(eee.getMessage(), eee);
            ErrorDialogUI.showError(eee);
            System.exit(1);
        }
    }
}
