/*
 * #%L
 * JMexico :: Swing Editor Demo
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor.demo;

import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.event.InputDesignFactorEvent;
import fr.reseaumexico.model.event.InputDesignFactorListener;
import fr.reseaumexico.model.event.InputDesignScenarioEvent;
import fr.reseaumexico.model.event.InputDesignScenarioListener;
import fr.reseaumexico.model.parser.InputDesignParser;
import fr.reseaumexico.model.writer.InputDesignXmlWriter;
import jaxx.runtime.swing.ErrorDialogUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFileChooser;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n._;

/**
 * Handler of main UI
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class MexicoEditorDemoUIHandler {

    /** Logger */
    private static Log log = LogFactory.getLog(MexicoEditorDemoUIHandler.class);

    private final MexicoEditorDemoUI ui;

    public MexicoEditorDemoUIHandler(MexicoEditorDemoUI ui) {
        this.ui = ui;
    }

    public void closeApplication() {
        ui.dispose();
    }

    public MexicoEditorDemoUI initUI() {

        // display
        ui.setVisible(true);

        // synch to error dialog
        ErrorDialogUI.init(ui);

        return ui;
    }

    public void openFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle(_("jmexico.file.open.dialog"));
        int returnVal = fileChooser.showOpenDialog(ui);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();

            loadInputFile(selectedFile);
        }
    }

    public void loadInputFile(File selectedFile) {

        if (log.isInfoEnabled()) {
            log.info("Load input file: " + selectedFile);
        }
        InputDesign inputDesign = readInputDesignFile(selectedFile);

        // add listener to listen option values changes
        inputDesign.addInputDesignFactorListener(new InputDesignFactorListener() {

            @Override
            public void factorValueChanged(InputDesignFactorEvent event) {
                ui.setInputDesign(event.getSource());
            }
        });
        // add listener to listen option values changes
        inputDesign.addInputDesignScenarioListener(new InputDesignScenarioListener() {

            @Override
            public void scenarioAdded(InputDesignScenarioEvent event) {
                ui.setInputDesign(event.getSource());
            }

            @Override
            public void scenarioRemoved(InputDesignScenarioEvent event) {
                ui.setInputDesign(event.getSource());
            }

            @Override
            public void scenarioRenamed(InputDesignScenarioEvent event) {
                ui.setInputDesign(event.getSource());
            }
        });

        ui.setInputDesign(inputDesign);

        // keep selected file
        ui.getModel().setInputDesignFile(selectedFile);
    }

    public void saveFile() {
        // get selected file
        File selectedFile = ui.getModel().getInputDesignFile();

        // save modified model
        try {
            InputDesignXmlWriter writer = new InputDesignXmlWriter(ui.getInputDesign());
            try {
                writer.write(selectedFile);
            } finally {
                ui.getModel().setOptionChanged(false);
            }
        } catch (IOException eee) {
            log.error("Failed to save inputDesign file '" + selectedFile.getName() + "'", eee);
            ErrorDialogUI.showError(eee);
        }
    }

    protected InputDesign readInputDesignFile(File selectedFile) {
        InputDesign result = null;
        try {
            InputDesignParser parser = new InputDesignParser();
            result = parser.getModel(selectedFile);

        } catch (Exception eee) {
            log.error("Failed to read inputDesign file '" + selectedFile.getName() + "'", eee);
            ErrorDialogUI.showError(eee);
        }

        return result;
    }
}
