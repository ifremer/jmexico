Démarrage de la démo
--------------------

Pour lancer la démo, taper la commande suivante dans une console :

./go.sh

ou

./go.bat

Vous pouvez aussi dans un explorateur double cliquer sur le fichier jar dans
ce même répertoire.

Note : Pour cela votre explorateur de fichier doit être configurer pour lancer
       des applications java. 
