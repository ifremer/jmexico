<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>fr.reseaumexico</groupId>
    <artifactId>jmexico</artifactId>
    <version>0.11.1</version>
  </parent>

  <artifactId>jmexico-editor-demo</artifactId>

  <name>JMexico :: Swing Editor Demo</name>
  <description>JMexico - Swing Editor Demo module</description>

  <properties>

    <maven.jar.main.class>
      fr.reseaumexico.editor.demo.RunMexicoEditorDemo
    </maven.jar.main.class>

    <!-- jaxx configuration -->
    <jaxx.addProjectClassPath>true</jaxx.addProjectClassPath>
    <jaxx.addSourcesToClassPath>true</jaxx.addSourcesToClassPath>
    <jaxx.autoImportCss>true</jaxx.autoImportCss>
    <jaxx.autoRecurseInCss>false</jaxx.autoRecurseInCss>

    <i18n.bundleOutputName>jmexico-editor-demo-i18n</i18n.bundleOutputName>

    <!-- generate license bundled files -->
    <license.generateBundle>true</license.generateBundle>

    <!-- Post Release configuration -->
    <skipPostRelease>false</skipPostRelease>

  </properties>

  <dependencies>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>jmexico-model</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>jmexico-editor</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>org.nuiton.i18n</groupId>
      <artifactId>nuiton-i18n</artifactId>
    </dependency>

    <!-- jaxx dependencies -->
    <dependency>
      <groupId>org.nuiton.jaxx</groupId>
      <artifactId>jaxx-runtime</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.jaxx</groupId>
      <artifactId>jaxx-widgets</artifactId>
    </dependency>

    <dependency>
      <groupId>org.swinglabs.swingx</groupId>
      <artifactId>swingx-common</artifactId>
    </dependency>

    <!-- logging dependencies -->
    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
    </dependency>

    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <scope>runtime</scope>
    </dependency>

  </dependencies>

  <build>
    <pluginManagement>
      <plugins>

        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <configuration>
            <archive>
              <manifest>
                <useUniqueVersions>true</useUniqueVersions>
                <addClasspath>true</addClasspath>
                <classpathPrefix>./lib/</classpathPrefix>
                <mainClass>${maven.jar.main.class}</mainClass>
              </manifest>
            </archive>
          </configuration>
        </plugin>

      </plugins>

    </pluginManagement>
    <plugins>

      <plugin>
        <groupId>org.nuiton.jaxx</groupId>
        <artifactId>jaxx-maven-plugin</artifactId>
        <executions>
          <execution>
            <goals>
              <goal>generate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-dependencies</id>
            <goals>
              <goal>copy-dependencies</goal>
            </goals>
            <configuration>
              <overWriteReleases>false</overWriteReleases>
              <overWriteSnapshots>true</overWriteSnapshots>
              <overWriteIfNewer>true</overWriteIfNewer>
              <outputDirectory>${project.build.directory}/lib</outputDirectory>
              <silent>true</silent>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>i18n-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>scan-sources</id>
            <goals>
              <goal>parserJava</goal>
              <goal>gen</goal>
            </goals>
            <configuration>
              <entries>
                <entry>
                  <basedir>${project.build.directory}/generated-sources/java/</basedir>
                </entry>
              </entries>
            </configuration>
          </execution>
          <execution>
            <id>make-bundle</id>
            <goals>
              <goal>bundle</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-licenses</id>
            <goals>
              <goal>update-project-license</goal>
              <goal>add-third-party</goal>
            </goals>
            <configuration>
              <licenseMerges>
                <licenseMerge>
                  The Apache Software License, Version 2.0|Apache License, Version 2.0
                </licenseMerge>
                <licenseMerge>BSD License|BSD</licenseMerge>
              </licenseMerges>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

  <profiles>

    <!-- by default jnlp is only perform on a release stage when using the maven-release-plugin -->
    <profile>
      <id>jnlp</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <build>

        <defaultGoal>verify</defaultGoal>
        <plugins>
          <!-- key store secrets availables -->
          <plugin>
            <groupId>org.nuiton</groupId>
            <artifactId>maven-helper-plugin</artifactId>
            <version>1.3</version>
            <executions>
              <execution>
                <id>get-keystore</id>
                <goals>
                  <goal>share-server-secret</goal>
                </goals>
                <phase>package</phase>
                <configuration>
                  <serverId>codelutin-keystore</serverId>
                  <privateKeyOut>keystorepath</privateKeyOut>
                  <passwordOut>keystorepass</passwordOut>
                  <usernameOut>keyalias</usernameOut>
                  <passphraseOut>keypass</passphraseOut>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <!-- make webstart  -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>webstart-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>generate-jnlp</id>
                <phase>package</phase>
                <goals>
                  <goal>jnlp-inline</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <id>assembly-profile</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <build>
        <defaultGoal>verify</defaultGoal>
        <plugins>
          <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <configuration>
              <descriptors>
                <descriptor>src/main/assembly/bin.xml</descriptor>
              </descriptors>
              <attach>false</attach>
            </configuration>
            <executions>
              <execution>
                <id>create-assemblies</id>
                <phase>verify</phase>
                <goals>
                  <goal>single</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

    <!-- by default jnlp is only perform on a release stage when using the maven-release-plugin -->
    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>webstart-maven-plugin</artifactId>
            <version>${webstartPluginVersion}</version>
          </plugin>
        </plugins>
      </reporting>

    </profile>
  </profiles>
</project>
