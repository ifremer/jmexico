/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model;

import com.google.common.collect.Maps;
import fr.reseaumexico.model.event.ScenarioFactorValueEvent;
import fr.reseaumexico.model.event.ScenarioFactorValueListener;

import javax.swing.event.EventListenerList;
import java.util.Map;

/**
 * Implementation of {@link Scenario} to provide listener API
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class ScenarioImpl extends Scenario {

    private static final long serialVersionUID = 1L;

    protected final EventListenerList factorListenerList;

    public ScenarioImpl() {
        factorListenerList = new EventListenerList();
    }

    @Override
    public Object getFactorValue(Factor factor) {
        Map<Factor, Object> factorValues = getFactorValues();
        if (factorValues == null) {
            return null;
        }
        return factorValues.get(factor);
    }

    @Override
    public void setFactorValue(Factor factor, Object value) {
        Map<Factor, Object> oldFactorValues = getFactorValues();
        if (factorValues == null) {
            factorValues = Maps.newHashMap();
        }
        Object oldValue = factorValues.get(factor);
        factorValues.put(factor, value);

        firePropertyChange(PROPERTY_FACTOR_VALUES, oldFactorValues, factorValues);
        fireFactorValueChanged(factor.getId(), oldValue, value);
    }

    @Override
    public void addFactorValueListener(ScenarioFactorValueListener factorListener) {
        factorListenerList.add(ScenarioFactorValueListener.class, factorListener);
    }

    @Override
    public void removeFactorValueListener(ScenarioFactorValueListener factorListener) {
        factorListenerList.remove(ScenarioFactorValueListener.class, factorListener);
    }

    protected void fireFactorValueChanged(String factorId, Object oldValue, Object newValue) {
        ScenarioFactorValueEvent event = new ScenarioFactorValueEvent(this, factorId, oldValue, newValue);
        ScenarioFactorValueListener[] listeners = factorListenerList.getListeners(ScenarioFactorValueListener.class);
        for (ScenarioFactorValueListener listener : listeners) {
            listener.factorValueChanged(event);
        }
    }

    @Override
    public String toString() {
        return getName();
    }
}
