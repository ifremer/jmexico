/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Factory for format all jmexico dates using {@link SimpleDateFormat}
 *
 * @author sletellier - letellier@codelutin.com
 * @see SimpleDateFormat
 * @since 0.1
 */
public class DateFormatFactory {

    public static final String MEXICO_DATE_PATTERN = "yyyy-MM-dd";

    protected static DateFormat mexicoDateFormat;

    public static DateFormat getMexicoDateFormat() {
        // singleton
        if (mexicoDateFormat == null) {
            mexicoDateFormat = new SimpleDateFormat(MEXICO_DATE_PATTERN);
        }
        return mexicoDateFormat;
    }
}
