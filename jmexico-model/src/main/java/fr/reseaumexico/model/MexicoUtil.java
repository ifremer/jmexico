/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model;

import com.google.common.base.Function;
import org.apache.commons.lang3.StringUtils;

/**
 * @author sletellier - letellier@codelutin.com
 */
public class MexicoUtil {

    public static final Function<Factor, String> GET_FACTOR_NAME = new Function<Factor, String>() {

        @Override
        public String apply(Factor input) {
            return input.getName();
        }
    };

    public static Object getTypedValue(ValueType type, String value) {
        Object result;
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        switch (type) {
            case INTEGER:
                result = Integer.parseInt(value);
                break;
            case BOOLEAN:
                result = Boolean.parseBoolean(value);
                break;
            case DECIMAL:
                result = Double.parseDouble(value);
                break;
            default:
                result = value;
                break;
            // TODO sletelier : convert string to matrice
            // case MATRICE:
        }
        return result;
    }
}
