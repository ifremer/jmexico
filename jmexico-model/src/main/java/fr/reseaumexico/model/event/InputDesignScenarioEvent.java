/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.event;

import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.Scenario;

import java.util.EventObject;

/**
 * Event fired when {@link Scenario} change on {@link InputDesign}
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class InputDesignScenarioEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    protected Scenario scenario;

    public InputDesignScenarioEvent(InputDesign inputDesign,
                                    Scenario scenario) {
        super(inputDesign);
        this.scenario = scenario;
    }

    @Override
    public InputDesign getSource() {
        return (InputDesign) super.getSource();
    }

    public Scenario getScenario() {
        return scenario;
    }
}
