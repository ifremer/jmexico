/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.event;

import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.Scenario;

/**
 * Event fired when {@link Factor} change on {@link InputDesign}
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class InputDesignFactorEvent extends InputDesignScenarioEvent {

    private static final long serialVersionUID = 1L;

    protected String factorId;

    protected Object factorOldValue;

    protected Object factorNewValue;

    public InputDesignFactorEvent(InputDesign inputDesign,
                                  Scenario scenario,
                                  String factorId,
                                  Object oldValue,
                                  Object newValue) {
        super(inputDesign, scenario);
        this.factorId = factorId;
        factorOldValue = oldValue;
        factorNewValue = newValue;
    }

    public String getFactorId() {
        return factorId;
    }

    public Object getFactorOldValue() {
        return factorOldValue;
    }

    public Object getFactorNewValue() {
        return factorNewValue;
    }
}
