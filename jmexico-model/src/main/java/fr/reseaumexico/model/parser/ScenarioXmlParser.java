package fr.reseaumexico.model.parser;

/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.MexicoUtil;
import fr.reseaumexico.model.Scenario;
import fr.reseaumexico.model.ScenarioImpl;
import fr.reseaumexico.model.ValueType;
import org.apache.commons.collections.CollectionUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static fr.reseaumexico.model.MexicoXmlConstant.FACTOR_ID;
import static fr.reseaumexico.model.MexicoXmlConstant.FACTOR_NAME;
import static fr.reseaumexico.model.MexicoXmlConstant.SCENARIO;
import static fr.reseaumexico.model.MexicoXmlConstant.SCENARIO_FACTOR_VALUES;
import static fr.reseaumexico.model.MexicoXmlConstant.SCENARIO_NAME;
import static fr.reseaumexico.model.MexicoXmlConstant.SCENARIO_ORDER_NUMBER;

/**
 * Parser for input design xml files, build a {@link Scenario} model.
 *
 * @author tchemit - chemit@codelutin.com
 * @since 0.7
 */
public class ScenarioXmlParser extends MexicoXmlParser<Scenario> {

    /**
     * Factors in input design model indexed by their id.
     *
     * @since 0.7
     */
    protected final Map<String, Factor> factors;

    /**
     * Factors by their name (only used in {@code standalone} mode to find
     * out a factor back if his id is not correct.
     */
    protected final Multimap<String, Factor> factorByName;

    /**
     * Flag to know if parser is used standalone (says to import a scenario) or
     * as part of input designer parser.
     *
     * @since 0.7
     */
    protected final boolean standalone;

    protected final List<String> unknownFactors;

    public ScenarioXmlParser(Map<String, Factor> factors) {
        this(factors, true);
    }

    public ScenarioXmlParser(Map<String, Factor> factors, boolean standalone) {
        this.factors = factors;
        this.standalone = standalone;
        this.unknownFactors = Lists.newArrayList();
        this.factorByName = ArrayListMultimap.create();
        if (standalone) {

            // populate the factorByName
            factorByName.putAll(Multimaps.index(factors.values(),
                                                MexicoUtil.GET_FACTOR_NAME));
        }
    }

    @Override
    protected Scenario parseModel(XmlPullParser parser) throws IOException, XmlPullParserException, ParseException {

        unknownFactors.clear();

        if (standalone) {
            // file must start with input design tag
            checkStartFile(
                    parser, SCENARIO,
                    "Scenario file must start with " + SCENARIO + " tag");
        }

        Scenario scenario = new ScenarioImpl();

        // name
        String name = parser.getAttributeValue(null, SCENARIO_NAME);
        scenario.setName(name);

        // orderValue
        String orderValue = parser.getAttributeValue(null, SCENARIO_ORDER_NUMBER);
        scenario.setOrderNumber(Integer.parseInt(orderValue));

        // factor values
        if (testNextStartTag(parser, SCENARIO_FACTOR_VALUES)) {

            Map<Factor, Object> factorValues = Maps.newLinkedHashMap();
            while (!(testCurrentEndTag(parser, SCENARIO))) {

                // get factor by his id
                String factorId = parser.getAttributeValue(null, FACTOR_ID);

                Factor factor = factors.get(factorId);

                if (factor == null) {
                    if (standalone) {

                        // fallback by name
                        String factorName =
                                parser.getAttributeValue(null, FACTOR_NAME);

                        Collection<Factor> possibleFactors =
                                factorByName.get(factorName);
                        if (CollectionUtils.isNotEmpty(possibleFactors)) {

                            // ok use the first one :(
                            factor = possibleFactors.iterator().next();
                        } else {

                            // really not found
                            // keep factor name
                            unknownFactors.add(factorName);

                            // read factor value
                            parser.nextText();

                            // read next tag
                            parser.nextTag();
                            continue;
                        }

                    } else {

                        // do not accept this
                        throw new IOException(
                                "Could not find factor named '" +
                                factorId + "' at line " + parser.getLineNumber());
                    }
                }
                ValueType valueType = factor.getDomain().getValueType();

                // factor value
                String value = parser.nextText();

                factorValues.put(factor, getTypedValue(valueType, value));

                // read next tag
                parser.nextTag();
            }
            scenario.setFactorValues(factorValues);
        }

        if (!standalone) {

            // read next tag
            parser.nextTag();
        }

        if (standalone) {

            // make sure all factors are loaded
            Set<Factor> usedFactors = scenario.getFactorValues().keySet();
            Set<Factor> allFactors = Sets.newHashSet(factors.values());

            allFactors.removeAll(usedFactors);

//            for (Factor factorToAdd : allFactors) {
//                scenario.setFactorValue(factorToAdd, 0.d);
//            }
        }

        return scenario;
    }

    public List<String> getUnknownFactors() {
        return Lists.newArrayList(unknownFactors);
    }
}
