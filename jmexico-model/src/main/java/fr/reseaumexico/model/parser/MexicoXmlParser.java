/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.parser;

import fr.reseaumexico.model.DateFormatFactory;
import fr.reseaumexico.model.MexicoUtil;
import fr.reseaumexico.model.ValueType;

import java.text.ParseException;
import java.util.Date;

/**
 * Abstract parser to get model from xml file using XPP3 for mexico
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public abstract class MexicoXmlParser<M> extends XmlParser<M> {

    public Date parseDate(String toParse) throws ParseException {
        return DateFormatFactory.getMexicoDateFormat().parse(toParse);
    }

    public Object getTypedValue(ValueType type, String value) {
        return MexicoUtil.getTypedValue(type, value);
    }
}
