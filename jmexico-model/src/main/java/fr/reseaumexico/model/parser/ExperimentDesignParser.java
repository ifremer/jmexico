/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.parser;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.reseaumexico.model.DistributionParameter;
import fr.reseaumexico.model.DistributionParameterImpl;
import fr.reseaumexico.model.Domain;
import fr.reseaumexico.model.DomainImpl;
import fr.reseaumexico.model.ExperimentDesign;
import fr.reseaumexico.model.ExperimentDesignImpl;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.FactorImpl;
import fr.reseaumexico.model.Feature;
import fr.reseaumexico.model.FeatureImpl;
import fr.reseaumexico.model.Level;
import fr.reseaumexico.model.LevelImpl;
import fr.reseaumexico.model.MexicoTechnicalException;
import fr.reseaumexico.model.MexicoXmlConstant;
import fr.reseaumexico.model.ValueType;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Parser for experiment design xml files, build a {@link ExperimentDesign} model.
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class ExperimentDesignParser extends MexicoXmlParser<ExperimentDesign> implements MexicoXmlConstant {

    /**
     * Cache of factors indexed by their id.
     *
     * @since 0.1
     */
    protected Map<String, Factor> factorCache;

    @Override
    protected ExperimentDesign parseModel(XmlPullParser parser) throws IOException, XmlPullParserException, ParseException {

        factorCache = Maps.newLinkedHashMap();

        ExperimentDesign model = new ExperimentDesignImpl();

        parseExperimentDesignMeta(parser, model);
        parseFactors(parser, model);

        return model;
    }

    protected void parseExperimentDesignMeta(XmlPullParser parser,
                                             ExperimentDesign model) throws IOException, XmlPullParserException, ParseException {

        // file must start with experiment design tag
        if (parser.nextTag() == XmlPullParser.START_TAG &&
            !parserEqual(parser, EXPERIMENT_DESIGN)) {
            throw new MexicoTechnicalException("Experiment design file must start with " + EXPERIMENT_DESIGN + " tag");
        }

        // parse experiment design id
        String id = parser.getAttributeValue(null, EXPERIMENT_DESIGN_ID);
        model.setId(id);

        // parse date
        String dateAsString = parser.getAttributeValue(null, EXPERIMENT_DESIGN_DATE);
        model.setDate(parseDate(dateAsString));

        // parse author
        String author = parser.getAttributeValue(null, EXPERIMENT_DESIGN_AUTHOR);
        model.setAuthor(author);

        // parse license
        String license = parser.getAttributeValue(null, EXPERIMENT_DESIGN_LICENCE);
        model.setLicence(license);

        // parse description
        if (testNextStartTag(parser, EXPERIMENT_DESIGN_DESCRIPTION)) {

            model.setDescription(parser.nextText());

            // read next tag
            parser.nextTag();
        }
    }

    protected void parseFactors(XmlPullParser parser,
                                ExperimentDesign model) throws IOException, XmlPullParserException {

        // factors
        if (testCurrentStartTag(parser, FACTORS)) {

            List<Factor> factors = Lists.newArrayList();
            while (!testNextEndTag(parser, FACTORS)) {

                // factor
                factors.add(parseFactor(parser));
            }
            model.setFactor(factors);
        }

        // read experience design next tag
        parser.nextTag();
    }

    protected Factor parseFactor(XmlPullParser parser) throws IOException, XmlPullParserException {

        Factor factor = new FactorImpl();

        // parse id
        String id = parser.getAttributeValue(null, FACTOR_ID);
        factor.setId(id);

        // parse name
        String name = parser.getAttributeValue(null, FACTOR_NAME);
        factor.setName(name);

        // while all child attributes is not parsed
        while (!(testCurrentEndTag(parser, FACTOR))) {

            // parse description
            if (testNextStartTag(parser, FACTOR_DESCRIPTION)) {
                factor.setDescription(parser.nextText());

                // read function close tag
                parser.nextTag();
            }

            // domain
            if (testCurrentStartTag(parser, DOMAIN)) {
                factor.setDomain(parseDomain(parser));
            }

            // features
            if (testCurrentStartTag(parser, FEATURE)) {

                List<Feature> features = Lists.newArrayList();

                while (parserEqual(parser, FEATURE)) {

                    // feature
                    features.add(parseFeature(parser));

                    // read feature close tag
                    parser.nextTag();

                    // read next tag
                    parser.nextTag();
                }
                factor.setFeature(features);
            }
        }

        // keep factors
        factorCache.put(factor.getId(), factor);

        return factor;
    }

    protected Domain parseDomain(XmlPullParser parser) throws XmlPullParserException, IOException {
        Domain domain = new DomainImpl();

        // name
        String domaineName = parser.getAttributeValue(null, DOMAIN_NAME);
        domain.setName(domaineName);

        // distributionName
        String distributionName = parser.getAttributeValue(null, DOMAIN_DISTRIBUTION_NAME);
        domain.setDistributionName(distributionName);

        // valueType
        String domaineValueType = parser.getAttributeValue(null, DOMAIN_VALUE_TYPE);
        ValueType domaineType = ValueType.valueOf(domaineValueType);
        domain.setValueType(domaineType);

        // nominaleValue
        String nominaleValue = parser.getAttributeValue(null, DOMAIN_NOMINAL_VALUE);

        domain.setNominalValue(getTypedValue(domaineType, nominaleValue));

        // while all child attributes is not parsed
        while (!(testCurrentEndTag(parser, DOMAIN))) {

            // distributionParameters
            if (testNextStartTag(parser, DOMAIN_DISTRIBUTION_PARAMETER)) {

                List<DistributionParameter> distributionParameters = Lists.newArrayList();
                while (parserEqual(parser, DOMAIN_DISTRIBUTION_PARAMETER)) {
                    DistributionParameter distributionParameter = new DistributionParameterImpl();

                    // name
                    String name = parser.getAttributeValue(null, DISTRIBUTION_PARAMETER_NAME);
                    distributionParameter.setName(name);

                    // valueType
                    String valueType = parser.getAttributeValue(null, DISTRIBUTION_PARAMETER_VALUE_TYPE);
                    ValueType type = ValueType.valueOf(valueType);
                    distributionParameter.setValueType(type);

                    // value
                    String value = parser.getAttributeValue(null, DISTRIBUTION_PARAMETER_VALUE);
                    distributionParameter.setValue(getTypedValue(type, value));

                    distributionParameters.add(distributionParameter);

                    // read close tag
                    parser.nextTag();

                    // read next start tag
                    parser.nextTag();
                }
                domain.setDistributionParameter(distributionParameters);
            }

            // level
            if (testCurrentStartTag(parser, DOMAIN_LEVEL)) {

                List<Level> levels = Lists.newArrayList();
                while (parserEqual(parser, DOMAIN_LEVEL)) {
                    Level level = new LevelImpl();

                    // value
                    String value = parser.getAttributeValue(null, LEVEL_VALUE);
                    level.setValue(value);

                    // weight
                    String weight = parser.getAttributeValue(null, LEVEL_WEIGHT);
                    level.setWeight(Integer.valueOf(weight));

                    levels.add(level);
                }
                domain.setLevels(levels);
            }
        }
        return domain;
    }

    protected Feature parseFeature(XmlPullParser parser) {
        Feature feature = new FeatureImpl();

        // name
        String name = parser.getAttributeValue(null, FEATURE_NAME);
        feature.setName(name);

        // value type
        String valueType = parser.getAttributeValue(null, FEATURE_VALUE_TYPE);
        ValueType type = ValueType.valueOf(valueType);
        feature.setValueType(type);

        // value
        String value = parser.getAttributeValue(null, FEATURE_VALUE);
        feature.setValue(getTypedValue(type, value));

        return feature;
    }

    public Map<String, Factor> getFactorCache() {
        return factorCache;
    }
}
