/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.parser;

import com.google.common.collect.Lists;
import fr.reseaumexico.model.ExperimentDesign;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.InputDesignImpl;
import fr.reseaumexico.model.Scenario;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import static fr.reseaumexico.model.MexicoXmlConstant.INPUT_DESIGN;
import static fr.reseaumexico.model.MexicoXmlConstant.INPUT_DESIGN_DATE;
import static fr.reseaumexico.model.MexicoXmlConstant.SCENARIO;

/**
 * Parser for input design xml files, build a {@link InputDesign} model.
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class InputDesignParser extends MexicoXmlParser<InputDesign> {

    protected ExperimentDesignParser experimentDesignParser;

    public InputDesignParser() {
        super();
        experimentDesignParser = new ExperimentDesignParser();
    }

    @Override
    protected InputDesign parseModel(XmlPullParser parser) throws IOException, XmlPullParserException, ParseException {
        InputDesign model = new InputDesignImpl();

        parseInputDesignMeta(parser, model);

        // parse experiment design
        ExperimentDesign experimentDesign = experimentDesignParser.parseModel(parser);
        model.setExperimentDesign(experimentDesign);

        parseScenarios(parser, model);

        return model;
    }

    protected void parseInputDesignMeta(XmlPullParser parser, InputDesign model) throws IOException, XmlPullParserException, ParseException {

        // file must start with input design tag
        checkStartFile(
                parser, INPUT_DESIGN,
                "Input design file must start with " + INPUT_DESIGN + " tag");

        // parse date
        String dateAsString = parser.getAttributeValue(null, INPUT_DESIGN_DATE);
        model.setDate(parseDate(dateAsString));
    }

    protected void parseScenarios(XmlPullParser parser, InputDesign model) throws IOException, XmlPullParserException, ParseException {

        // scenarios
        if (testNextStartTag(parser, SCENARIO)) {

            ScenarioXmlParser scenarioXmlParser =
                    new ScenarioXmlParser(
                            experimentDesignParser.getFactorCache(), false);

            List<Scenario> scenarios = Lists.newArrayList();
            while (!(testCurrentEndTag(parser, INPUT_DESIGN))) {

                Scenario scenario = scenarioXmlParser.parseModel(parser);
                scenarios.add(scenario);
            }
            model.setScenario(scenarios);
        }
    }
}
