package fr.reseaumexico.model.parser;

/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.csvreader.CsvReader;
import com.google.common.base.Charsets;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.io.Files;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.MexicoUtil;
import fr.reseaumexico.model.Scenario;
import fr.reseaumexico.model.ScenarioImpl;
import fr.reseaumexico.model.ValueType;
import org.apache.commons.collections.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Parser for input design csv files, build a {@link Scenario} model.
 */
public class ScenarioCSVParser {

    /**
     * Factors in input design model indexed by their id.
     *
     * @since 0.7
     */
    protected final Map<String, Factor> factors;

    /**
     * Factors by their name (only used in {@code standalone} mode to find
     * out a factor back if his id is not correct.
     */
    protected final Multimap<String, Factor> factorByName;

    protected final List<String> unknownFactors;

    public ScenarioCSVParser(Map<String, Factor> factors) {
        this.factors = factors;
        this.unknownFactors = new ArrayList<String>();
        this.factorByName = ArrayListMultimap.create();
        // populate the factorByName
        factorByName.putAll(Multimaps.index(factors.values(), MexicoUtil.GET_FACTOR_NAME));
    }

    public Scenario getModel(File file) throws IOException {

        unknownFactors.clear();

        Scenario scenario = new ScenarioImpl();

        CsvReader csvReader = null;
        try {

            // csv file doesn't contains headers, so it's not a real
            // csv file
            csvReader = new CsvReader(Files.newReader(file, Charsets.UTF_8), ',');

            Map<Factor, Object> factorValues = Maps.newLinkedHashMap();
            while (csvReader.readRecord()) {

                // factor name
                String factorName = csvReader.get(0);

                Factor factor = factors.get(factorName);

                if (factor == null) {
                    // check if factor is possible
                    Collection<Factor> possibleFactors = factorByName.get(factorName);
                    if (CollectionUtils.isNotEmpty(possibleFactors)) {
                        // ok use the first one :(
                        factor = possibleFactors.iterator().next();
                    } else {

                        // really not found
                        // keep factor name
                        unknownFactors.add(factorName);
                        continue;
                    }
                }

                ValueType valueType = factor.getDomain().getValueType();

                // factor value
                String value = csvReader.get(1);

                factorValues.put(factor, MexicoUtil.getTypedValue(valueType, value));
            }
            scenario.setFactorValues(factorValues);

        } finally {
            if (csvReader != null) {
                csvReader.close();
            }
        }

        return scenario;
    }

    public List<String> getUnknownFactors() {
        return Lists.newArrayList(unknownFactors);
    }
}
