/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.parser;

import com.google.common.base.Charsets;
import com.google.common.io.Closeables;
import com.google.common.io.Files;
import fr.reseaumexico.model.MexicoTechnicalException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;

/**
 * Abstract parser to get model from xml file using XPP3
 *
 * @author sletellier - letellier@codelutin.com
 * @see XmlPullParser
 * @since 0.1
 */
public abstract class XmlParser<M> {

    public M getModel(File file) throws XmlPullParserException, IOException, ParseException {

        Reader reader = Files.newReader(file, Charsets.UTF_8);

        try {

            // create xpp parser
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(reader);

            // parse model
            M result = parseModel(parser);

            // close reader
            reader.close();

            // return model
            return result;
        } finally {

            // make sure model is closed
            Closeables.closeQuietly(reader);
        }
    }

    protected abstract M parseModel(XmlPullParser parser) throws IOException, XmlPullParserException, ParseException;

    public boolean parserEqual(XmlPullParser parser, String name) {
        String tagName = parser.getName();
        return tagName.equals(name);
    }

    /**
     * Check that parser starts with the given tag.
     *
     * @param parser       xpp parser used
     * @param requiredTag  required first tag
     * @param errorMessage error message to send if required tag was not found
     * @throws IOException
     * @throws XmlPullParserException
     */
    protected void checkStartFile(XmlPullParser parser,
                                  String requiredTag,
                                  String errorMessage) throws IOException, XmlPullParserException {

        // file must start with input design tag
        if (parser.next() == XmlPullParser.START_TAG &&
            !parserEqual(parser, requiredTag)) {
            throw new MexicoTechnicalException(errorMessage);
        }
    }

    protected boolean testNextStartTag(XmlPullParser parser,
                                       String requiredTag) throws IOException, XmlPullParserException {
        return parser.nextTag() == XmlPullParser.START_TAG &&
               parserEqual(parser, requiredTag);
    }

    protected boolean testNextEndTag(XmlPullParser parser,
                                     String requiredTag) throws IOException, XmlPullParserException {
        return parser.nextTag() == XmlPullParser.END_TAG &&
               parserEqual(parser, requiredTag);
    }

    protected boolean testCurrentEndTag(XmlPullParser parser,
                                        String requiredTag) throws IOException, XmlPullParserException {
        return parser.getEventType() == XmlPullParser.END_TAG &&
               parserEqual(parser, requiredTag);
    }

    protected boolean testCurrentStartTag(XmlPullParser parser,
                                          String requiredTag) throws IOException, XmlPullParserException {
        return parser.getEventType() == XmlPullParser.START_TAG &&
               parserEqual(parser, requiredTag);
    }
}
