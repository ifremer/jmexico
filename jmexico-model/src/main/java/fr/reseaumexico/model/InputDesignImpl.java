/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.reseaumexico.model.event.InputDesignFactorEvent;
import fr.reseaumexico.model.event.InputDesignFactorListener;
import fr.reseaumexico.model.event.InputDesignScenarioEvent;
import fr.reseaumexico.model.event.InputDesignScenarioListener;
import fr.reseaumexico.model.event.ScenarioFactorValueEvent;
import fr.reseaumexico.model.event.ScenarioFactorValueListener;

import javax.swing.event.EventListenerList;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Map;

/**
 * Implementation of {@link InputDesign} to provide listener API
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class InputDesignImpl extends InputDesign {

    private static final long serialVersionUID = 1L;

    protected final EventListenerList listenerList;

    protected final Map<Scenario, ScenarioFactorValueListener> scenarioFactorValueListenerMap;

    public InputDesignImpl() {
        scenarioFactorValueListenerMap = Maps.newHashMap();
        listenerList = new EventListenerList();
        addPropertyChangeListener(PROPERTY_SCENARIO, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Scenario newValue = (Scenario) propertyChangeEvent.getNewValue();
                Scenario oldValue = (Scenario) propertyChangeEvent.getOldValue();
                if (newValue == null) {
                    // was removed
                    fireInputDesignScenarioRemoved(oldValue);
                }

                if (oldValue == null) {
                    // was added
                    fireInputDesignScenarioAdded(newValue);
                }
            }
        });
    }

    @Override
    public void addScenario(final Scenario scenarios) {
        if (scenario == null) {
            scenario = Lists.newArrayList();
        }
        super.addScenario(scenarios);

        ScenarioFactorValueListener scenarioFactorValueListener = new ScenarioFactorValueListener() {

            @Override
            public void factorValueChanged(ScenarioFactorValueEvent event) {
                fireFactorChanged(scenarios, event.getFactorId(), event.getFactorOldValue(), event.getFactorNewValue());
            }
        };

        // keep listener
        scenarioFactorValueListenerMap.put(scenarios, scenarioFactorValueListener);
        scenarios.addFactorValueListener(scenarioFactorValueListener);
    }

    @Override
    public void renameScenario(Scenario scenario, String newName) {
        scenario.setName(newName);
        fireInputDesignScenarioRenamed(scenario);
    }

    @Override
    public void addAllScenario(Collection<Scenario> scenarios) {
        for (Scenario scenario : scenarios) {
            addScenario(scenario);
        }
    }

    @Override
    public void setScenario(Collection<Scenario> scenarios) {
        addAllScenario(scenarios);
    }

    @Override
    public boolean removeScenario(Scenario scenario) {

        // Remove listener
        scenario.removeFactorValueListener(scenarioFactorValueListenerMap.get(scenario));
        scenarioFactorValueListenerMap.remove(scenario);

        return super.removeScenario(scenario);
    }

    @Override
    public boolean removeAllScenario(Collection<Scenario> scenarios) {
        boolean removed = false;
        for (Scenario scenario : scenarios) {
            removed = removeScenario(scenario);
        }
        return removed;
    }

    @Override
    public void addInputDesignScenarioListener(InputDesignScenarioListener scenarioListener) {
        listenerList.add(InputDesignScenarioListener.class, scenarioListener);
    }

    @Override
    public void removeInputDesignScenarioListener(InputDesignScenarioListener scenarioListener) {
        listenerList.remove(InputDesignScenarioListener.class, scenarioListener);
    }

    @Override
    public void addInputDesignFactorListener(InputDesignFactorListener factorListener) {
        listenerList.add(InputDesignFactorListener.class, factorListener);
    }

    @Override
    public void removeInputDesignFactorListener(InputDesignFactorListener factorListener) {
        listenerList.remove(InputDesignFactorListener.class, factorListener);
    }

    protected void fireInputDesignScenarioAdded(Scenario scenario) {
        InputDesignScenarioEvent event = new InputDesignScenarioEvent(this, scenario);
        InputDesignScenarioListener[] listeners = listenerList.getListeners(InputDesignScenarioListener.class);
        for (InputDesignScenarioListener listener : listeners) {
            listener.scenarioAdded(event);
        }
    }

    protected void fireInputDesignScenarioRenamed(Scenario scenario) {
        InputDesignScenarioEvent event = new InputDesignScenarioEvent(this, scenario);
        InputDesignScenarioListener[] listeners = listenerList.getListeners(InputDesignScenarioListener.class);
        for (InputDesignScenarioListener listener : listeners) {
            listener.scenarioRenamed(event);
        }
    }

    protected void fireInputDesignScenarioRemoved(Scenario scenario) {
        InputDesignScenarioEvent event = new InputDesignScenarioEvent(this, scenario);
        InputDesignScenarioListener[] listeners = listenerList.getListeners(InputDesignScenarioListener.class);
        for (InputDesignScenarioListener listener : listeners) {
            listener.scenarioRemoved(event);
        }
    }

    protected void fireFactorChanged(Scenario scenario, String factorId, Object oldValue, Object newValue) {
        InputDesignFactorEvent event = new InputDesignFactorEvent(this, scenario, factorId, oldValue, newValue);
        InputDesignFactorListener[] listeners = listenerList.getListeners(InputDesignFactorListener.class);
        for (InputDesignFactorListener listener : listeners) {
            listener.factorValueChanged(event);
        }
    }
}
