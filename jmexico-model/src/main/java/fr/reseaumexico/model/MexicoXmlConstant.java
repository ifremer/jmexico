/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model;

import java.beans.Introspector;

/**
 * Regroup all mexico xml constants tags
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public interface MexicoXmlConstant {

    String EXPERIMENT_DESIGN = Introspector.decapitalize(ExperimentDesign.class.getSimpleName());

    String EXPERIMENT_DESIGN_DATE = ExperimentDesign.PROPERTY_DATE;

    String EXPERIMENT_DESIGN_ID = ExperimentDesign.PROPERTY_ID;

    String EXPERIMENT_DESIGN_AUTHOR = ExperimentDesign.PROPERTY_AUTHOR;

    String EXPERIMENT_DESIGN_LICENCE = ExperimentDesign.PROPERTY_LICENCE;

    String EXPERIMENT_DESIGN_DESCRIPTION = ExperimentDesign.PROPERTY_DESCRIPTION;

    String FACTORS = "factors";

    String FACTOR = Introspector.decapitalize(Factor.class.getSimpleName());

    String FACTOR_ID = Factor.PROPERTY_ID;

    String FACTOR_NAME = Factor.PROPERTY_NAME;

    String FACTOR_UNIT = Factor.PROPERTY_UNIT;

    String FACTOR_DESCRIPTION = Factor.PROPERTY_DESCRIPTION;

    String DOMAIN = Factor.PROPERTY_DOMAIN;

    String DOMAIN_NAME = Domain.PROPERTY_NAME;

    String DOMAIN_DISTRIBUTION_NAME = Domain.PROPERTY_DISTRIBUTION_NAME;

    String DOMAIN_DISTRIBUTION_PARAMETER = Introspector.decapitalize(DistributionParameter.class.getSimpleName());

    String DISTRIBUTION_PARAMETER_NAME = DistributionParameter.PROPERTY_NAME;

    String DISTRIBUTION_PARAMETER_VALUE = DistributionParameter.PROPERTY_VALUE;

    String DISTRIBUTION_PARAMETER_VALUE_TYPE = DistributionParameter.PROPERTY_VALUE_TYPE;

    String DOMAIN_LEVEL = Introspector.decapitalize(Level.class.getSimpleName());

    String LEVEL_VALUE = Level.PROPERTY_VALUE;

    String LEVEL_WEIGHT = Level.PROPERTY_WEIGHT;

    String DOMAIN_VALUE_TYPE = Domain.PROPERTY_VALUE_TYPE;

    String DOMAIN_NOMINAL_VALUE = Domain.PROPERTY_NOMINAL_VALUE;

    String FEATURE = Introspector.decapitalize(Feature.class.getSimpleName());

    String FEATURE_NAME = Feature.PROPERTY_NAME;

    String FEATURE_VALUE = Feature.PROPERTY_VALUE;

    String FEATURE_VALUE_TYPE = Feature.PROPERTY_VALUE_TYPE;

    String INPUT_DESIGN = Introspector.decapitalize(InputDesign.class.getSimpleName());

    String INPUT_DESIGN_DATE = InputDesign.PROPERTY_DATE;

    String SCENARIO = Introspector.decapitalize(Scenario.class.getSimpleName());

    String SCENARIO_NAME = Scenario.PROPERTY_NAME;

    String SCENARIO_ORDER_NUMBER = Scenario.PROPERTY_ORDER_NUMBER;

    String SCENARIO_FACTOR_VALUES = Scenario.PROPERTY_FACTOR_VALUES;
}
