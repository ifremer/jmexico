/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.writer;

import fr.reseaumexico.model.DistributionParameter;
import fr.reseaumexico.model.Domain;
import fr.reseaumexico.model.ExperimentDesign;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.Level;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

/**
 * Writer to create experiment design file with {@link ExperimentDesign} model
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class ExperimentDesignXmlWriter extends MexicoXmlWriter<ExperimentDesign> {

    public ExperimentDesignXmlWriter(ExperimentDesign model) throws IOException {
        super(model);
    }

    @Override
    public XmlNode getRootElement() {

        XmlNode rootXmlNode = new XmlNode(EXPERIMENT_DESIGN);
        composeExperimentDesignMeta(rootXmlNode);
        composeFactors(rootXmlNode);

        // TODO sletellier 2011/12/14 : implement workflow
        // composeWorkFlow(rootXmlNode);

        return rootXmlNode;
    }

    protected void composeExperimentDesignMeta(XmlNode rootXmlNode) {

        // date
        Date date = model.getDate();
        addParameter(rootXmlNode, EXPERIMENT_DESIGN_DATE, formatDate(date));

        addParameter(rootXmlNode, EXPERIMENT_DESIGN_ID, model.getId());
        addParameter(rootXmlNode, EXPERIMENT_DESIGN_AUTHOR, model.getAuthor());
        addParameter(rootXmlNode, EXPERIMENT_DESIGN_LICENCE, model.getLicence());

        // description
        String description = model.getDescription();
        if (StringUtils.isNotEmpty(description)) {
            XmlNode.createElement(rootXmlNode, EXPERIMENT_DESIGN_DESCRIPTION, description);
        }
    }

    protected void composeFactors(XmlNode rootXmlNode) {
        Collection<Factor> factors = model.getFactor();

        // do nothing if no factors in model
        if (factors.isEmpty()) {
            return;
        }

        // creating factors node
        XmlNode factorsXmlNode = XmlNode.createElement(rootXmlNode, FACTORS);
        for (Factor factor : factors) {

            // factor
            XmlNode factorXmlNode = XmlNode.createElement(factorsXmlNode, FACTOR);
            addParameter(factorXmlNode, FACTOR_ID, factor.getId());
            addParameter(factorXmlNode, FACTOR_NAME, factor.getName());
            addParameter(factorXmlNode, FACTOR_UNIT, factor.getUnit());

            // description
            String description = model.getDescription();
            if (StringUtils.isNotEmpty(description)) {
                XmlNode.createElement(factorXmlNode, FACTOR_DESCRIPTION, factor.getDescription());
            }

            // domain
            Domain domain = factor.getDomain();
            if (domain != null) {
                XmlNode domainXmlNode = XmlNode.createElement(factorXmlNode, DOMAIN);
                addParameter(domainXmlNode, DOMAIN_NAME, domain.getName());
                addParameter(domainXmlNode, DOMAIN_VALUE_TYPE, domain.getValueType());
                addParameter(domainXmlNode, DOMAIN_NOMINAL_VALUE, domain.getNominalValue());
                addParameter(domainXmlNode, DOMAIN_DISTRIBUTION_NAME, domain.getDistributionName());

                // distributionParameters
                Collection<DistributionParameter> distributionParameters = domain.getDistributionParameter();
                if (distributionParameters != null) {
                    for (DistributionParameter distributionParameter : distributionParameters) {
                        XmlNode distributionParameterXmlNode = XmlNode.createElement(domainXmlNode, DOMAIN_DISTRIBUTION_PARAMETER);
                        addParameter(distributionParameterXmlNode, DISTRIBUTION_PARAMETER_NAME, distributionParameter.getName());
                        addParameter(distributionParameterXmlNode, DISTRIBUTION_PARAMETER_VALUE, distributionParameter.getValue());
                        addParameter(distributionParameterXmlNode, DISTRIBUTION_PARAMETER_VALUE_TYPE, distributionParameter.getValueType());
                    }
                }

                // levels
                Collection<Level> levels = domain.getLevels();
                if (levels != null) {
                    for (Level level : levels) {
                        XmlNode distributionParameterXmlNode = XmlNode.createElement(domainXmlNode, DOMAIN_LEVEL);
                        addParameter(distributionParameterXmlNode, LEVEL_VALUE, level.getValue());
                        addParameter(distributionParameterXmlNode, LEVEL_WEIGHT, level.getWeight());
                    }
                }
            }

            // features
            addAllFeature(factorXmlNode, factor.getFeature());
        }
    }
}
