/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.writer;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Represent an XML node
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class XmlNode {

    public static final String XML_META = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    public static final String CLOSED_TAG = "%s<%s%s/>\n";

    public static final String START_TAG = "%s<%s%s>";

    public static final String END_TAG = "%s</%s>\n";

    protected List<XmlNode> children;

    protected Map<String, String> attributes;

    protected String tagName;

    protected String text;

    public XmlNode(String tagName) {
        this(tagName, null);
    }

    public XmlNode(String tagName, String text) {
        this.tagName = tagName;
        this.text = text;
        children = Lists.newArrayList();
        attributes = Maps.newHashMap();
    }

    public void add(XmlNode e) {
        children.add(e);
    }

    public void addAttribute(String name, String value) {
        attributes.put(name, value);
    }

    public String toXml(int indent) {
        StringBuilder stringBuilder = new StringBuilder(XML_META).append("\n");
        buildXml(-indent, indent, stringBuilder);
        return stringBuilder.toString();
    }

    protected void buildXml(int start, int indent, StringBuilder stringBuilder) {

        // closed tag if content is empty
        if (children.isEmpty() && StringUtils.isEmpty(text)) {
            addToBuilder(stringBuilder, CLOSED_TAG, getIndentPrefix(start, indent), tagName);
            return;
        }

        // start tag
        addToBuilder(stringBuilder, START_TAG, getIndentPrefix(start, indent), tagName);
        if (StringUtils.isNotEmpty(text)) {

            // end inline tag
            addToBuilder(stringBuilder, END_TAG, StringEscapeUtils.escapeXml(text), tagName);
        } else {
            stringBuilder.append("\n");
            for (XmlNode child : children) {
                child.buildXml(start + indent, indent, stringBuilder);
            }
            // end tag
            addToBuilder(stringBuilder, END_TAG, getIndentPrefix(start, indent), tagName);
        }
    }

    protected void addToBuilder(StringBuilder stringBuilder, String template, String prefix, String tagName) {
        stringBuilder.append(String.format(template, prefix, tagName, getAttributesAsString()));
    }

    protected String getIndentPrefix(int start, int lenght) {
        String result = "";
        for (int i = 0; i < start + lenght; i++) {
            result += " ";
        }
        return result;
    }

    protected String getAttributesAsString() {
        StringBuilder builder = new StringBuilder();
        Set<Map.Entry<String, String>> entries = attributes.entrySet();
        for (Map.Entry<String, String> entry : entries) {

            // add to builder param like : name="value"
            builder.append(" ")
                    .append(entry.getKey())
                    .append("=\"")
                    .append(StringEscapeUtils.escapeXml(entry.getValue()))
                    .append("\"");
        }
        return builder.toString();
    }

    public static XmlNode createElement(XmlNode parentXmlNode, String tagName) {
        return createElement(parentXmlNode, tagName, null);
    }

    public static XmlNode createElement(XmlNode parentXmlNode, String tagName, String text) {
        XmlNode xmlNode = createElement(tagName, text);
        parentXmlNode.add(xmlNode);
        return xmlNode;
    }

    public static XmlNode createElement(String tagName, String text) {
        return new XmlNode(tagName, text);
    }
}
