package fr.reseaumexico.model.writer;

/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.Scenario;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Writes a scenario from a input design as a xml file.
 *
 * @author tchemit - chemit@codelutin.com
 * @since 0.7
 */
public class ScenarioXmlWriter extends MexicoXmlWriter<Scenario> {

    public static void write(Scenario model, File exportFile) throws IOException {
        ScenarioXmlWriter writer = new ScenarioXmlWriter(model, true);
        writer.write(exportFile);
    }

    /**
     * A flag to qualify when the writer is used only to write a single
     * scenario (scenario export).
     *
     * @since 0.8
     */
    private final boolean standalone;

    public ScenarioXmlWriter(Scenario model, boolean standalone) {
        super(model);
        this.standalone = standalone;
    }

    @Override
    public XmlNode getRootElement() {
        XmlNode rootXmlNode = new XmlNode(SCENARIO);
        addParameter(rootXmlNode, Scenario.PROPERTY_NAME, model.getName());
        addParameter(rootXmlNode, Scenario.PROPERTY_ORDER_NUMBER, model.getOrderNumber());

        // factor  values
        Map<Factor, Object> factorValues = model.getFactorValues();
        for (Map.Entry<Factor, Object> entry : factorValues.entrySet()) {
            Object value = entry.getValue();
            XmlNode factorValueXmlNode = XmlNode.createElement(
                    rootXmlNode, Scenario.PROPERTY_FACTOR_VALUES, String.valueOf(value));

            Factor factor = entry.getKey();

            factorValueXmlNode.addAttribute(FACTOR_ID, factor.getId());
            if (standalone) {

                // add also the name of factor (used to manage error when
                // reading back file)
                factorValueXmlNode.addAttribute(FACTOR_NAME, factor.getName());
            }
        }

        // features
        addAllFeature(rootXmlNode, model.getFeature());
        return rootXmlNode;
    }
}
