/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.writer;

import fr.reseaumexico.model.DateFormatFactory;
import fr.reseaumexico.model.Feature;
import fr.reseaumexico.model.MexicoXmlConstant;

import java.util.Collection;
import java.util.Date;

/**
 * Abstract writer create mexico files with specific model
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public abstract class MexicoXmlWriter<M> extends XmlWriter<M> implements MexicoXmlConstant {

    protected MexicoXmlWriter(M model) {
        super(model);
    }

    public void addParameter(XmlNode xmlNode, String parameterName, Object value) {
        if (value != null) {
            xmlNode.addAttribute(parameterName, value.toString());
        }
    }

    protected void addAllFeature(XmlNode factorXmlNode, Collection<Feature> features) {
        if (features != null) {
            for (Feature feature : features) {
                XmlNode featureXmlNode = XmlNode.createElement(factorXmlNode, FEATURE);
                addParameter(featureXmlNode, FEATURE_NAME, feature.getName());
                addParameter(featureXmlNode, FEATURE_VALUE, feature.getValue());
                addParameter(featureXmlNode, FEATURE_VALUE_TYPE, feature.getValueType());
            }
        }
    }

    public String formatDate(Date toFormat) {
        return DateFormatFactory.getMexicoDateFormat().format(toFormat);
    }
}
