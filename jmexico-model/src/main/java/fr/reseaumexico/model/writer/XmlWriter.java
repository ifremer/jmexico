/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2016 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.writer;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.io.IOUtils;

/**
 * Abstract writer to create xml files using {@link XmlNode}
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public abstract class XmlWriter<M> {

    public static final int INDENT = 2;

    protected M model;

    protected abstract XmlNode getRootElement();

    protected XmlWriter(M model) {
        this.model = model;
    }

    public void write(File file) throws IOException {

        Writer writer = Files.newWriter(file, Charsets.UTF_8);
        try {

            // get root xml node
            XmlNode rootElement = getRootElement();

            // transform it to string
            String toWrite = rootElement.toXml(INDENT);

            // write it into file
            writer.write(toWrite);

            // close writer
            writer.close();
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

}
