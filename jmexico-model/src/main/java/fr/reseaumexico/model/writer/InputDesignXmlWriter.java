/*
 * #%L
 * JMexico :: Model
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.model.writer;

import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.Scenario;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

/**
 * Writer to create input design file with {@link InputDesign} model
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class InputDesignXmlWriter extends MexicoXmlWriter<InputDesign> {

    protected ExperimentDesignXmlWriter experimentDesignXmlWriter;

    public InputDesignXmlWriter(InputDesign model) throws IOException {
        super(model);
        experimentDesignXmlWriter = new ExperimentDesignXmlWriter(model.getExperimentDesign());
    }

    @Override
    public XmlNode getRootElement() {
        XmlNode rootXmlNode = new XmlNode(INPUT_DESIGN);
        composeInputDesignMeta(rootXmlNode);
        composeScenarios(rootXmlNode);

        return rootXmlNode;
    }

    protected void composeInputDesignMeta(XmlNode rootXmlNode) {

        // date
        Date date = model.getDate();
        addParameter(rootXmlNode, INPUT_DESIGN_DATE, formatDate(date));

        // experiement design
        rootXmlNode.add(experimentDesignXmlWriter.getRootElement());
    }

    protected void composeScenarios(XmlNode rootXmlNode) {

        // scenarios
        Collection<Scenario> scenarios = model.getScenario();
        if (scenarios != null) {
            for (Scenario scenario : scenarios) {

                ScenarioXmlWriter scenarioXmlWriter = new ScenarioXmlWriter(scenario, false);

                XmlNode scenariosXmlNode = scenarioXmlWriter.getRootElement();
                rootXmlNode.add(scenariosXmlNode);
            }
        }
    }
}
