package fr.reseaumexico.editor;

/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Model for clone scenario operation.
 *
 * @author tchemit - chemit@codelutin.com
 * @since 0.7
 */
public class CloneScenarioModel extends AbstractScenarioModel<CloneScenarioModel> {

    private static final long serialVersionUID = 1L;

    protected String scenarioName;

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        Object oldValue = this.scenarioName;
        this.scenarioName = scenarioName;
        firePropertyChange("scenarioName", oldValue, scenarioName);
    }

    @Override
    public void copyTo(CloneScenarioModel model) {
        super.copyTo(model);
        model.setScenarioName(getScenarioName());
    }
}
