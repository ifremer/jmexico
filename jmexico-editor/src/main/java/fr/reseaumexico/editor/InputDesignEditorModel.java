package fr.reseaumexico.editor;

/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.MexicoUtil;
import fr.reseaumexico.model.Scenario;
import fr.reseaumexico.model.ValueType;
import fr.reseaumexico.model.event.InputDesignScenarioEvent;
import fr.reseaumexico.model.event.InputDesignScenarioListener;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.jdesktop.beans.AbstractSerializableBean;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n._;

/**
 * Model of {@link InputDesignEditor} UI.
 *
 * @author tchemit - chemit@codelutin.com
 * @since 0.7
 */
public class InputDesignEditorModel extends AbstractSerializableBean implements InputDesignScenarioListener {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_INPUT_DESIGN = "inputDesign";

    public static final String PROPERTY_SCENARIO_EXISTS = "scenarioExists";

    protected InputDesign inputDesign;

    protected List<Factor> factors;

    protected final AbstractTableModel tableModel;

    public InputDesignEditorModel() {
        tableModel = new InputDesignTableModel();
    }

    public InputDesign getInputDesign() {
        return inputDesign;
    }

    public boolean isScenarioExists() {
        return inputDesign != null &&
               CollectionUtils.isNotEmpty(inputDesign.getScenario());
    }

    public TableModel getTableModel() {
        return tableModel;
    }

    public List<Factor> getFactors() {
        return factors;
    }

    public void setInputDesign(InputDesign inputDesign) {
        InputDesign oldValue = this.inputDesign;

        if (oldValue != null) {
            inputDesign.removeInputDesignScenarioListener(this);
        }
        this.inputDesign = inputDesign;
        inputDesign.addInputDesignScenarioListener(this);

        // extract factors
        factors = Lists.newLinkedList(
                inputDesign.getExperimentDesign().getFactor());

        firePropertyChange(PROPERTY_INPUT_DESIGN, oldValue, inputDesign);

        fireScenarioModified();
    }

    @Override
    public void scenarioAdded(InputDesignScenarioEvent event) {
        fireScenarioModified();
    }

    @Override
    public void scenarioRemoved(InputDesignScenarioEvent event) {
        fireScenarioModified();
    }

    @Override
    public void scenarioRenamed(InputDesignScenarioEvent event) {
        fireScenarioModified();
    }

    public Collection<Scenario> getScenario() {
        return inputDesign == null ? null : inputDesign.getScenario();
    }

    protected void fireScenarioModified() {
        firePropertyChange(PROPERTY_SCENARIO_EXISTS, null, isScenarioExists());
        tableModel.fireTableStructureChanged();
    }

    protected class InputDesignTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 1L;

        @Override
        public int getRowCount() {
            return factors == null ? 0 : factors.size();
        }

        @Override
        public int getColumnCount() {
            int result = 0;
            if (inputDesign != null) {
                result = 1;
                Collection<Scenario> scenarios = inputDesign.getScenario();

                if (scenarios != null) {
                    result = scenarios.size() + 1;
                }
            }
            return result;
        }

        @Override
        public String getColumnName(int rowIndex) {
            String result = null;
            if (rowIndex == 0) {
                result = _("jmexico.factor.name");
            } else if (inputDesign != null) {

                Scenario scenario = inputDesign.getScenario(rowIndex - 1);
                result = scenario == null ? "" : scenario.getName();
            }
            return result;
        }

        @Override
        public Class<?> getColumnClass(int rowIndex) {
            Class<?> result = Object.class;
            if (rowIndex == 0) {
                result = Factor.class;
            }
            return result;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return inputDesign != null && columnIndex > 0;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object result = null;

            if (inputDesign != null) {

                // get key on rowIndex
                Factor factor = factors.get(rowIndex);

                // first column is factors
                if (columnIndex == 0) {
                    result = factor;
                } else {

                    // others are scenarios
                    columnIndex = columnIndex - 1;
                    Scenario scenario = inputDesign.getScenario(columnIndex);
                    if (scenario != null) {

                        Map<Factor, Object> factorValues =
                                scenario.getFactorValues();

                        result = factorValues.get(factor);
                    }
                }
            }
            return result;
        }

        @Override
        public void setValueAt(Object o, int rowIndex, int columnIndex) {

            if (inputDesign != null && columnIndex > 0) {

                Scenario scenario = inputDesign.getScenario(columnIndex - 1);

                // get key if rowIndex
                Factor factor = factors.get(rowIndex);

                // take care of type
                ValueType valueType = factor.getDomain().getValueType();

                String toStringValue = ObjectUtils.toString(o);
                Object value = MexicoUtil.getTypedValue(valueType, toStringValue);
                scenario.setFactorValue(factor, value);

                fireTableCellUpdated(rowIndex, columnIndex);
            }
        }
    }
}
