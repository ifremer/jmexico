/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor.factorValue;

import fr.reseaumexico.model.Factor;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Cell editor for factor values
 *
 * @author sletellier - letellier@codelutin.com
 * @see TableCellEditor
 * @since 0.1
 */
public class FactorValueCellEditor extends AbstractCellEditor
        implements TableCellEditor, FocusListener {

    private static final long serialVersionUID = 1L;

    /**
     * Logger
     */
    private static Log log = LogFactory.getLog(FactorValueCellEditor.class);

    protected JTable table;

    protected Factor factor;

    protected Object currentValue;

    protected JTextField defaultInlineEditor;

    protected FactorValueEditorFactory.FactorValueCellEditor specificEditor;

    protected boolean isEditing;

    protected FactorValueEditorFactory factory;

    public FactorValueCellEditor(JAXXContext context) {
        defaultInlineEditor = new JTextField();
        defaultInlineEditor.addFocusListener(this);

        // search specific factory in context
        factory = context.getContextValue(FactorValueEditorFactory.class);

        // if not found, use mexico one
        if (factory == null) {
            factory = new FactorValueEditorFactory();
        }
    }

    @Override
    public Object getCellEditorValue() {
        if (isEditing) {
            currentValue = defaultInlineEditor.getText();
            isEditing = false;
            fireEditingStopped();
        }
        if (specificEditor != null) {
            currentValue = specificEditor.getValue();
        }
        return currentValue;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // get factor
        factor = (Factor) table.getValueAt(row, 0);

        currentValue = value;

        specificEditor = factory.getCellEditor(factor, currentValue);

        if (specificEditor != null) {
            Component component = specificEditor.getComponent();

            // stop editing on select for combos
            if (component instanceof JComboBox) {
                ((JComboBox) component).addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        fireEditingStopped();
                    }
                });
            }

            return component;
        }
        defaultInlineEditor.setText(currentValue == null ? "" : String.valueOf(currentValue));
        return defaultInlineEditor;
    }

    @Override
    public void focusGained(FocusEvent e) {

        // get factor
        FactorValueEditor editor = factory.getDialogEditor(factor, currentValue);
        if (editor == null) {
            isEditing = true;
        } else {
            // display ui
            editor.setVisible(true);
            currentValue = editor.getValue();
            defaultInlineEditor.setText(currentValue == null ? "" : String.valueOf(currentValue));
            fireEditingStopped();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        fireEditingStopped();
    }
}
