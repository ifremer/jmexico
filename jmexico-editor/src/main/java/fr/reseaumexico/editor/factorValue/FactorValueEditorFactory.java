/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor.factorValue;

import com.google.common.collect.Maps;
import fr.reseaumexico.model.Domain;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.ValueType;
import jaxx.runtime.swing.editor.NumberEditor;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextField;
import java.awt.Component;
import java.util.Map;

/**
 * Factory used to open specific editor to input factor value
 *
 * @author sletellier - letellier@codelutin.com
 * @since 0.1
 */
public class FactorValueEditorFactory {

    protected Map<Factor, FactorValueCellEditor<?>> factorValueEditorCache = Maps.newHashMap();

    /**
     * Provide specific {@link JDialog} editor for factor value
     *
     * @param factor concerned
     * @param value  value to edit
     * @return {@link FactorValueEditor} contain editor to open as {@link JDialog},
     * null if cell editor must be used
     */
    public FactorValueEditor getDialogEditor(Factor factor, Object value) {

        // TODO sletellier 20121221 : open specific editor

//        if (oldValue == null) {
//
//            JAXXInitialContext context = new JAXXInitialContext();
//            context.add(factor);
//            FactorValueEditor<B> editor = (FactorValueEditor<B>) new FactorValueEditorImpl(context);
//            editor.setValue(oldValue);
//            return editor;
//        }

        return null;
    }

    protected FactorValueCellEditor<?> getFactorValueCellEditor(Factor factor) {
        return factorValueEditorCache.get(factor);
    }

    protected void setFactorValueCellEditor(Factor factor, FactorValueCellEditor<?> editor) {
        factorValueEditorCache.put(factor, editor);
    }

    /**
     * Provide specific cell editor for factor value
     *
     * @param factor concerned
     * @param value  value to edit
     * @return {@link FactorValueCellEditor} contain editor to display
     */
    public FactorValueCellEditor getCellEditor(Factor factor, Object value) {

        FactorValueCellEditor<?> factorValueCellEditor = getFactorValueCellEditor(factor);

        // get type
        Domain domain = factor.getDomain();
        ValueType valueType = ValueType.STRING;
        if (domain != null) {
            valueType = domain.getValueType();
        }

        if (factorValueCellEditor == null) {
            switch (valueType) {
                case INTEGER:
                    factorValueCellEditor = new IntegerFactorValueInlineEditor();
                    break;
                case DECIMAL:
                    factorValueCellEditor = new DecimalFactorValueInlineEditor();
                    break;
                case BOOLEAN:
                    factorValueCellEditor = new BooleanFactorValueInlineEditor();
                    break;
                default:
                    factorValueCellEditor = new DefaultFactorValueInlineEditor();
            }
        }

        setFactorValueCellEditor(factor, factorValueCellEditor);

        factorValueCellEditor.setValue(value);
        return factorValueCellEditor;
    }

    /**
     * Provide a specific cell renderer for factor value
     *
     * @param factor concerned
     * @param value  value to edit
     * @return {@link FactorValueCellRenderer} contain renderer to display
     */
    public FactorValueCellRenderer getRenderedComponent(Factor factor, Object value) {
        return getCellEditor(factor, value);
    }

    public void clearCache() {
        factorValueEditorCache = Maps.newHashMap();
    }

    public interface FactorValueCellRenderer {

        Component getComponent();

        void setValue(Object value);
    }

    public interface FactorValueCellEditor<B> extends FactorValueCellRenderer {

        B getValue();

        void setValue(Object value);

        Component getComponent();
    }

    protected static class DefaultFactorValueInlineEditor implements FactorValueCellEditor<String> {

        protected JTextField component;

        public DefaultFactorValueInlineEditor() {
            component = new JTextField();
            component.setBorder(null);
        }

        @Override
        public void setValue(Object value) {
            component.setText(getStringValue(value));
        }

        @Override
        public String getValue() {
            return component.getText();
        }

        @Override
        public Component getComponent() {
            return component;
        }
    }

    protected static class BooleanFactorValueInlineEditor implements FactorValueCellEditor<Boolean> {

        protected JCheckBox checkBox;

        public BooleanFactorValueInlineEditor() {
            checkBox = new JCheckBox();
        }

        @Override
        public Boolean getValue() {
            return checkBox.isSelected();
        }

        @Override
        public void setValue(Object value) {
            if (value != null) {
                checkBox.setSelected((Boolean) value);
            }
        }

        @Override
        public Component getComponent() {
            return checkBox;
        }
    }

    protected static class IntegerFactorValueInlineEditor implements FactorValueCellEditor<Integer> {

        protected NumberEditor numberEditor;

        public IntegerFactorValueInlineEditor() {
            numberEditor = new NumberEditor();
            numberEditor.getTextField().setBorder(null);

            numberEditor.setUseFloat(false);
            numberEditor.setUseSign(true);
        }

        @Override
        public Integer getValue() {
            return (Integer) numberEditor.getModel();
        }

        @Override
        public void setValue(Object value) {
            // FIXME sletellier 20120105 : we dont need to set modelText
            numberEditor.setModel((Integer) value);
            numberEditor.setModelText(getStringValue(value));
        }

        @Override
        public Component getComponent() {
            return numberEditor;
        }
    }

    protected static class DecimalFactorValueInlineEditor implements FactorValueCellEditor<Double> {

        protected NumberEditor numberEditor;

        public DecimalFactorValueInlineEditor() {
            numberEditor = new NumberEditor();
            numberEditor.getTextField().setBorder(null);

            numberEditor.setModelType(Double.class);
            numberEditor.setUseFloat(true);
            numberEditor.setUseSign(true);
        }

        @Override
        public Double getValue() {
            return (Double) numberEditor.getModel();
        }

        @Override
        public void setValue(Object value) {
            numberEditor.setModel((Double) value);
            // FIXME sletellier 20120105 : we dont need to set modelText
            numberEditor.setModelText(getStringValue(value));
        }

        @Override
        public Component getComponent() {
            return numberEditor;
        }
    }

    protected static String getStringValue(Object value) {
        String text = String.valueOf(value);
        if (value == null) {
            text = StringUtils.EMPTY;
        }
        return text;
    }
}
