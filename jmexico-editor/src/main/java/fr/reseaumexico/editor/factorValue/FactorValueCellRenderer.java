/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor.factorValue;

import fr.reseaumexico.model.Factor;
import jaxx.runtime.JAXXContext;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * @author sletellier - letellier@codelutin.com
 */
public class FactorValueCellRenderer implements TableCellRenderer {

    protected FactorValueEditorFactory factory;

    protected TableCellRenderer delegate;

    public FactorValueCellRenderer(JAXXContext context) {

        // search specific factory in context
        factory = context.getContextValue(FactorValueEditorFactory.class);

        // if not found, use mexico one
        if (factory == null) {
            factory = new FactorValueEditorFactory();
        }

        delegate = new DefaultTableCellRenderer();
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
        Factor factor = (Factor) table.getModel().getValueAt(row, 0);

        FactorValueEditorFactory.FactorValueCellRenderer specificRenderer = factory.getRenderedComponent(factor, value);

        Component result;
        if (specificRenderer == null) {
            result = delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        } else {
            result = specificRenderer.getComponent();
        }

        return result;
    }
}
