/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import fr.reseaumexico.editor.factorValue.FactorValueCellEditor;
import fr.reseaumexico.editor.factorValue.FactorValueCellRenderer;
import fr.reseaumexico.editor.factorValue.FactorValueEditorFactory;
import fr.reseaumexico.model.Factor;
import fr.reseaumexico.model.InputDesign;
import fr.reseaumexico.model.Scenario;
import fr.reseaumexico.model.ScenarioImpl;
import fr.reseaumexico.model.parser.ScenarioCSVParser;
import fr.reseaumexico.model.parser.ScenarioXmlParser;
import fr.reseaumexico.model.writer.ScenarioXmlWriter;
import jaxx.runtime.swing.editor.FileEditor;
import jaxx.runtime.swing.renderer.DecoratorProviderTableCellRenderer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n._;

/**
 * @author sletellier - letellier@codelutin.com
 * @author tchemit - chemit@codelutin.com
 * @since 0.1
 */
public class InputDesignEditorHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(InputDesignEditorHandler.class);

    private final InputDesignEditor ui;

    protected Collection<ImportScenarioListener> importScenarioListeners = new LinkedList<ImportScenarioListener>();

    public InputDesignEditorHandler(InputDesignEditor ui) {
        this.ui = ui;
    }

    public void initUI() {

        ui.getModel().addPropertyChangeListener(InputDesignEditorModel.PROPERTY_INPUT_DESIGN, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                FactorValueEditorFactory factorValueEditorFactory =
                        ui.getContextValue(FactorValueEditorFactory.class);
                if (factorValueEditorFactory != null) {
                    factorValueEditorFactory.clearCache();
                }
            }
        });

        JXTable table = ui.getInputDesignTable();

        // table renderer
        table.setDefaultRenderer(Factor.class, new FactorNameCellRenderer());
        table.setDefaultRenderer(Object.class, new FactorValueCellRenderer(ui));

        // cell editor
        table.setDefaultEditor(Object.class, new FactorValueCellEditor(ui));
    }

    public void addScenario() {

        String selectedName = JOptionPane.showInputDialog(
                ui,
                _("jmexico.label.scenario.add.name"),
                _("jmexico.title.scenario.add"),
                JOptionPane.QUESTION_MESSAGE);

        // check that name is filled and is available

        boolean valid = StringUtils.isNotBlank(selectedName) &&
                        isScenarioNameAvailable(selectedName);

        if (valid) {

            int maxOrder = getMaxOrder();

            // create new scenario
            Scenario scenarioToAdd = new ScenarioImpl();

            // set last order
            // TODO sletellier 20111219 : ask user order number
            scenarioToAdd.setOrderNumber(maxOrder + 1);

            // set name
            scenarioToAdd.setName(selectedName);

            InputDesign inputDesign = ui.getInputDesign();

            // create all empty factor values
            Collection<Factor> factors = inputDesign.getExperimentDesign().getFactor();
            Map<Factor, Object> factorValues = Maps.newLinkedHashMap();
            for (Factor factor : factors) {
                factorValues.put(factor, null);
            }
            scenarioToAdd.setFactorValues(factorValues);

            inputDesign.addScenario(scenarioToAdd);
        }
    }

    public void renameScenario() {

        InputDesign inputDesign = ui.getInputDesign();

        RenameScenarioModel model = new RenameScenarioModel();
        model.setScenarios(inputDesign.getScenario());

        boolean accept = showRenameScenarioUI(model);

        if (accept) {

            // rename scenario
            Collection<Scenario> scenarios = inputDesign.getScenario();
            for (Scenario scenario : scenarios) {
                if (scenario.getName().equals(model.getScenarioName())) {
                    JOptionPane.showMessageDialog(ui,
                                                  _("jmexico.action.scenario.rename.alreadyexists", model.getScenarioName()),
                                                  _("jmexico.title.scenario.rename"),
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            inputDesign.renameScenario(model.getSelectedScenario(), model.getScenarioName());
        }
    }

    public void removeScenario() {
        InputDesign inputDesign = ui.getInputDesign();

        // show scenario select dialog
        Collection<Scenario> scenarios = inputDesign.getScenario();
        Scenario scenario = (Scenario) JOptionPane.showInputDialog(
                ui,
                _("jmexico.label.scenario.remove.selected"),
                _("jmexico.title.scenario.remove"),
                JOptionPane.QUESTION_MESSAGE,
                null,
                scenarios.toArray(),
                null);

        // if scenario is selected
        if (scenario != null) {
            inputDesign.removeScenario(scenario);
        }
    }

    public void cloneScenario() {

        InputDesign inputDesign = ui.getInputDesign();

        CloneScenarioModel model = new CloneScenarioModel();
        model.setScenarios(inputDesign.getScenario());

        boolean accept = showCloneScenarioUI(model);

        if (accept) {

            // clone scenario

            int maxOrder = getMaxOrder();

            Scenario scenarioToAdd = new ScenarioImpl();

            // set last order
            // TODO sletellier 20111219 : ask user order number
            scenarioToAdd.setOrderNumber(maxOrder + 1);

            // set name
            scenarioToAdd.setName(model.getScenarioName());

            Scenario selectedScenario = model.getSelectedScenario();

            // create all factor values (clone selected scenario values)
            Collection<Factor> factors = inputDesign.getExperimentDesign().getFactor();
            Map<Factor, Object> factorValues = Maps.newLinkedHashMap();
            for (Factor factor : factors) {
                Object factorValue = selectedScenario.getFactorValue(factor);

                factorValues.put(factor, factorValue);
            }
            scenarioToAdd.setFactorValues(factorValues);

            inputDesign.addScenario(scenarioToAdd);
        }
    }

    public void addScenarioImportListener(ImportScenarioListener l) {
        importScenarioListeners.add(l);
    }

    public void removeScenarioImportListener(ImportScenarioListener l) {
        importScenarioListeners.remove(l);
    }

    public void importScenario() {

        for (ImportScenarioListener l : importScenarioListeners) {
            l.beforeImportScenario();
        }

        InputDesign inputDesign = ui.getInputDesign();

        ImportScenarioModel model = new ImportScenarioModel();
        model.setScenarios(inputDesign.getScenario());

        boolean accept = showImportScenarioUI(model);

        if (accept) {

            // import scenario

            File importFile = model.getImportFile();

            String scenarioName = model.getScenarioName();

            if (log.isInfoEnabled()) {
                log.info("Import scenario '" + scenarioName + "' from " + importFile);
            }
            List<Factor> factors = ui.getModel().getFactors();


            // test file type
            if (importFile.getName().endsWith("." + _("jmexico.config.scenario.extension.csv"))) {
                importCSVScenario(factors, importFile, scenarioName);
            } else {
                importXMLScenario(factors, importFile, scenarioName);
            }
        }

        for (ImportScenarioListener l : importScenarioListeners) {
            l.afterImportScenario();
        }
    }

    protected void importCSVScenario(List<Factor> factors, File importFile, String scenarioName) {
        Map<String, Factor> factorsNameMap = Maps.uniqueIndex(factors, new Function<Factor, String>() {

            @Override
            public String apply(Factor input) {
                return input.getName();
            }
        });

        InputDesign inputDesign = ui.getInputDesign();
        ScenarioCSVParser parser = new ScenarioCSVParser(factorsNameMap);
        try {
            Scenario scenario = parser.getModel(importFile);

            scenario.setName(scenarioName);
            scenario.setOrderNumber(getMaxOrder());
            inputDesign.addScenario(scenario);

            List<String> unknownFactors = parser.getUnknownFactors();
            if (CollectionUtils.isNotEmpty(unknownFactors)) {
                StringBuilder sb = new StringBuilder();
                for (String unknownFactor : unknownFactors) {
                    sb.append("\n'").append(unknownFactor).append('\'');
                }
                JOptionPane.showMessageDialog(
                        ui,
                        _("jmexico.warning.factor.not.imported",
                          unknownFactors.size(), sb.toString()),
                        _("jmexico.title.importScenario"),
                        JOptionPane.WARNING_MESSAGE);
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not import scenario", e);
            }
        }
    }

    /**
     * Parse import file as XML.
     *
     * @param factors      factors
     * @param importFile   import file
     * @param scenarioName scenario name
     */
    protected void importXMLScenario(List<Factor> factors, File importFile, String scenarioName) {

        Map<String, Factor> factorsMap = Maps.uniqueIndex(factors, new Function<Factor, String>() {

            @Override
            public String apply(Factor input) {
                return input.getId();
            }
        });

        InputDesign inputDesign = ui.getInputDesign();
        ScenarioXmlParser parser = new ScenarioXmlParser(factorsMap);
        try {
            Scenario scenario = parser.getModel(importFile);

            scenario.setName(scenarioName);
            scenario.setOrderNumber(getMaxOrder());
            inputDesign.addScenario(scenario);

            List<String> unknownFactors = parser.getUnknownFactors();
            if (CollectionUtils.isNotEmpty(unknownFactors)) {
                StringBuilder sb = new StringBuilder();
                for (String unknownFactor : unknownFactors) {
                    sb.append("\n'").append(unknownFactor).append('\'');
                }
                JOptionPane.showMessageDialog(
                        ui,
                        _("jmexico.warning.factor.not.imported",
                          unknownFactors.size(), sb.toString()),
                        _("jmexico.title.importScenario"),
                        JOptionPane.WARNING_MESSAGE);
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not import scenario", e);
            }
        }
    }

    public void exportScenario() {

        InputDesign inputDesign = ui.getInputDesign();

        ExportScenarioModel model = new ExportScenarioModel();
        model.setScenarios(inputDesign.getScenario());

        boolean accept = showExportScenarioUI(model);

        if (accept) {

            // export scenario

            File exportFile = model.getExportFile(getScenarioExtension());
            Scenario scenario = model.getSelectedScenario();

            if (log.isInfoEnabled()) {
                log.info("Export scenario '" + scenario.getName() +
                         "' to file " + exportFile);
            }
            try {
                ScenarioXmlWriter.write(scenario, exportFile);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not export scenario to " + exportFile, e);
                }
            }
        }
    }

    public static void onSelectedScenarioChanged(AbstractScenarioModel<?> model,
                                                 ItemEvent event) {

        if (event.getStateChange() == ItemEvent.SELECTED) {
            Scenario item = (Scenario) event.getItem();
            model.setSelectedScenario(item);
        }
    }

    public static String getScenarioExtension() {
        return _("jmexico.config.scenario.extension");
    }

    protected boolean showCloneScenarioUI(CloneScenarioModel model) {

        // show ui

        CloneScenarioPanel panel = new CloneScenarioPanel();
        panel.init(model);

        int response = JOptionPane.showConfirmDialog(
                ui,
                panel,
                _("jmexico.title.scenario.clone"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean doIt = response == JOptionPane.OK_OPTION;

        if (doIt) {

            // user ask to perform operation

            //copy back model
            panel.getModel().copyTo(model);

            // validate model
            boolean valid = true;

            Scenario selectedScenario = model.getSelectedScenario();
            if (selectedScenario == null) {

                // no scenario selected
                JOptionPane.showMessageDialog(
                        ui,
                        _("jmexico.error.scenario.not.selected"),
                        _("jmexico.title.error"),
                        JOptionPane.ERROR_MESSAGE);

                valid = false;
            }

            if (valid) {

                // check scenario name is not used
                String selectedName = model.getScenarioName();

                if (StringUtils.isBlank(selectedName)) {

                    valid = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("jmexico.error.scenario.name.required"),
                            _("jmexico.title.error"),
                            JOptionPane.ERROR_MESSAGE);

                } else {
                    valid = isScenarioNameAvailable(selectedName);
                }
            }

            doIt = valid || showCloneScenarioUI(model);
        }

        return doIt;
    }

    protected boolean showRenameScenarioUI(RenameScenarioModel model) {

        // show ui

        RenameScenarioPanel panel = new RenameScenarioPanel();
        panel.init(model);

        int response = JOptionPane.showConfirmDialog(
                ui,
                panel,
                _("jmexico.title.scenario.rename"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean doIt = response == JOptionPane.OK_OPTION;

        if (doIt) {

            // user ask to perform operation

            //copy back model
            panel.getModel().copyTo(model);

            // validate model
            boolean valid = true;

            Scenario selectedScenario = model.getSelectedScenario();
            if (selectedScenario == null) {

                // no scenario selected
                JOptionPane.showMessageDialog(
                        ui,
                        _("jmexico.error.scenario.not.selected"),
                        _("jmexico.title.error"),
                        JOptionPane.ERROR_MESSAGE);

                valid = false;
            }

            if (valid) {

                // check scenario name is not used
                String selectedName = model.getScenarioName();

                if (StringUtils.isBlank(selectedName)) {

                    valid = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("jmexico.error.scenario.name.required"),
                            _("jmexico.title.error"),
                            JOptionPane.ERROR_MESSAGE);

                } else {
                    valid = isScenarioNameAvailable(selectedName);
                }
            }

            doIt = valid || showRenameScenarioUI(model);
        }

        return doIt;
    }

    protected boolean showExportScenarioUI(ExportScenarioModel model) {

        // show ui

        ExportScenarioPanel panel = new ExportScenarioPanel();
        panel.init(model);

        FileEditor fileEditor = panel.getExportDirectoryEditor();
        fileEditor.setDirectoryEnabled(true);
        fileEditor.setFileEnabled(false);

        int response = JOptionPane.showConfirmDialog(
                ui,
                panel,
                _("jmexico.title.scenario.export"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean doIt = response == JOptionPane.OK_OPTION;

        if (doIt) {

            // user ask to perform operation

            //copy back model
            panel.getModel().copyTo(model);

            // validate model
            boolean valid = true;

            Scenario selectedScenario = model.getSelectedScenario();
            if (selectedScenario == null) {

                // no scenario selected
                JOptionPane.showMessageDialog(
                        ui,
                        _("jmexico.error.scenario.not.selected"),
                        _("jmexico.title.error"),
                        JOptionPane.ERROR_MESSAGE);

                valid = false;
            }

            if (valid) {

                //check export directory is filled
                File exportDirectory = model.getExportDirectory();
                if (exportDirectory == null) {
                    valid = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("jmexico.error.scenario.exportDirectory.required"),
                            _("jmexico.title.error"),
                            JOptionPane.ERROR_MESSAGE);

                }
            }

            if (valid) {

                // check export filename  is filled
                String exportFilename = model.getExportFilename();

                if (StringUtils.isBlank(exportFilename)) {
                    valid = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("jmexico.error.scenario.exportFilename.required"),
                            _("jmexico.title.error"),
                            JOptionPane.ERROR_MESSAGE);

                }
            }

            doIt = valid || showExportScenarioUI(model);
        }
        return doIt;
    }

    protected boolean showImportScenarioUI(ImportScenarioModel model) {

        // show ui

        ImportScenarioPanel panel = new ImportScenarioPanel();
        panel.init(model);

        FileEditor fileEditor = panel.getImportFileEditor();

        fileEditor.setAcceptAllFileFilterUsed(false);
        fileEditor.setExts(getScenarioExtension() + "," + _("jmexico.config.scenario.extension.csv"));
        fileEditor.setExtsDescription(_("jmexico.config.scenario.extension.description") + "," +
                                      _("jmexico.config.scenario.extension.csv.description"));

        int response = JOptionPane.showConfirmDialog(
                ui,
                panel,
                _("jmexico.title.scenario.import"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean doIt = response == JOptionPane.OK_OPTION;

        if (doIt) {

            // user ask to perform operation

            //copy back model
            panel.getModel().copyTo(model);

            // validate model
            boolean valid = true;


            // check import file filled
            File importFile = model.getImportFile();

            if (importFile == null) {
                valid = false;
                JOptionPane.showMessageDialog(
                        ui,
                        _("jmexico.error.scenario.importFile.required"),
                        _("jmexico.title.error"),
                        JOptionPane.ERROR_MESSAGE);
            }

            if (valid) {

                // check scenario name is not used
                String selectedName = model.getScenarioName();

                if (StringUtils.isBlank(selectedName)) {

                    valid = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("jmexico.error.scenario.name.required"),
                            _("jmexico.title.error"),
                            JOptionPane.ERROR_MESSAGE);

                } else {
                    valid = isScenarioNameAvailable(selectedName);
                }
            }
            doIt = valid || showImportScenarioUI(model);
        }
        return doIt;
    }

    protected int getMaxOrder() {
        int maxOrder = 0;

        Collection<Scenario> scenarios = ui.getModel().getScenario();

        if (CollectionUtils.isNotEmpty(scenarios)) {
            for (Scenario scenario : scenarios) {
                int orderNumber = scenario.getOrderNumber();
                maxOrder = Math.max(maxOrder, orderNumber);
            }
        }
        return maxOrder;
    }

    protected boolean isScenarioNameAvailable(String selectedName) {
        boolean result = true;

        Collection<Scenario> scenarios = ui.getModel().getScenario();

        if (CollectionUtils.isNotEmpty(scenarios)) {
            for (Scenario scenario : scenarios) {
                if (selectedName.equalsIgnoreCase(scenario.getName())) {
                    result = false;
                    JOptionPane.showMessageDialog(
                            ui,
                            _("jmexico.error.scenario.name.used", selectedName),
                            _("jmexico.title.error"),
                            JOptionPane.ERROR_MESSAGE);
                    break;
                }
            }
        }
        return result;
    }
}
