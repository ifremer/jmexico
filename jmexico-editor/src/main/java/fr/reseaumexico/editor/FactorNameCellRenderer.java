/*
 * #%L
 * JMexico :: Swing Editor
 * %%
 * Copyright (C) 2016 Réseau Mexico, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.reseaumexico.editor;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.lang3.StringUtils;

import fr.reseaumexico.model.Factor;

/**
 * Factor cell renderer for editor table.
 * 
 * Handle factor name and fctor description as tooltip.
 * 
 * @author Eric Chatellier
 */
public class FactorNameCellRenderer extends DefaultTableCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = 8036762402104794846L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasfocus, int row, int column) {

        Factor factor = (Factor)value;

        String factorTxt = factor.getName();
        if (StringUtils.isEmpty(factorTxt)) {
            // fallback on id
            factorTxt = factor.getId();
        }
        setText(factorTxt);

        setToolTipText(factor.getDescription());

        return this;
    }
}
