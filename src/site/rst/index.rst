.. -
.. * #%L
.. * JMexico
.. * %%
.. * Copyright (C) 2011 - 2015 Réseau Mexico, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=======
JMexico
=======

:Authors: Tony Chemit <chemit@codelutin.com>

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2

Présentation
------------

Projet utilisant les formats mexico via le language java.

Le projet est composé de deux modules :

- **jmexico-model** qui définit le modèle java de l'*experiment design* et de
  l'*input design*. Ce module offre aussi une api pour lire et
  écrire au format xml ces deux types de document.

- **jmexico-swing-editor** définit une éditeur de fichier *input design*
  (ajouter/supprimer des scénarii, modifier la valeurs des facteurs).

Fichiers
--------

Voici quelques exemples de fichiers xml :

- un fichier de description de modèle ( `experimentDesign`_ )
- un fichier de données d'entrée ( `inputDesign`_ )

Démo
----

Une petite démo permet de tester l'éditeur. Pour l'utiliser suivre le guide de
démarrage de la démo (lien dans le menu).

Ensuite ouvrir l'input design d'exemple...

.. _experimentDesign: ./examples/experimentDesign.xml
.. _inputDesign: ./examples/inputDesign.xml
