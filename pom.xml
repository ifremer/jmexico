<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>codelutinpom</artifactId>
    <version>5.1</version>
  </parent>

  <groupId>fr.reseaumexico</groupId>
  <artifactId>jmexico</artifactId>
  <version>0.11.1</version>
  <packaging>pom</packaging>

  <name>JMexico</name>
  <description>
    Implantation de la norme Mexico en Java (http://reseau-mexico.fr/).
  </description>
  <url>http://jmexico.codelutin.com</url>
  <inceptionYear>2011</inceptionYear>
  <organization>
    <name>Code Lutin</name>
  </organization>

  <developers>
    <developer>
      <id>sletellier</id>
      <name>Sylvain Letellier</name>
      <email>letellier at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <roles>
        <role>Developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>
    <developer>
      <id>tchemit</id>
      <name>Tony Chemit</name>
      <email>chemit at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <roles>
        <role>Developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>
  </developers>

  <modules>
    <module>jmexico-model</module>
    <module>jmexico-editor</module>
    <module>jmexico-editor-demo</module>
  </modules>

  <scm>
    <url>https://gitweb.codelutin.com/jmexico.git</url>
    <connection>scm:git:http://git.codelutin.com/jmexico.git</connection>
    <developerConnection>scm:git:https://git.codelutin.com/jmexico.git</developerConnection>
  </scm>
  <distributionManagement>
    <site>
      <id>${site.server}</id>
      <url>${site.url}</url>
    </site>
  </distributionManagement>

  <properties>

    <projectId>jmexico</projectId>

    <!-- libraries version -->
    <nuitonI18nVersion>2.5</nuitonI18nVersion>
    <nuitonUtilsVersion>2.6.9</nuitonUtilsVersion>
    <eugenePluginVersion>2.6.1</eugenePluginVersion>
    <jaxxVersion>2.5.10</jaxxVersion>
    <swingXVersion>1.6.5-1</swingXVersion>

    <license.organizationName>Réseau Mexico, Codelutin</license.organizationName>
    <maven.deploy.skip>false</maven.deploy.skip>
  </properties>

  <dependencyManagement>
    <dependencies>

      <!-- nuiton dependencies -->
      <dependency>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-utils</artifactId>
        <version>${nuitonUtilsVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>nuiton-i18n</artifactId>
        <version>${nuitonI18nVersion}</version>
      </dependency>

      <!-- jaxx dependencies -->
      <dependency>
        <groupId>org.nuiton.jaxx</groupId>
        <artifactId>jaxx-runtime</artifactId>
        <version>${jaxxVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.nuiton.jaxx</groupId>
        <artifactId>jaxx-widgets</artifactId>
        <version>${jaxxVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.swinglabs.swingx</groupId>
        <artifactId>swingx-common</artifactId>
        <version>${swingXVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.swinglabs.swingx</groupId>
        <artifactId>swingx-core</artifactId>
        <version>${swingXVersion}</version>
      </dependency>

      <!-- Commons -->
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-lang3</artifactId>
        <version>3.4</version>
      </dependency>
      <dependency>
        <groupId>commons-logging</groupId>
        <artifactId>commons-logging</artifactId>
        <version>1.2</version>
      </dependency>
      <dependency>
        <groupId>commons-collections</groupId>
        <artifactId>commons-collections</artifactId>
        <version>3.2.1</version>
      </dependency>
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-collections4</artifactId>
        <version>4.0</version>
      </dependency>
      <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>2.4</version>
      </dependency>

      <dependency>
        <groupId>com.google.guava</groupId>
        <artifactId>guava</artifactId>
        <version>19.0</version>
      </dependency>

      <!-- xml parser -->
      <dependency>
        <groupId>xpp3</groupId>
        <artifactId>xpp3</artifactId>
        <version>1.1.4c</version>
      </dependency>

      <!-- Csv parser -->
      <dependency>
          <groupId>net.sourceforge.javacsv</groupId>
          <artifactId>javacsv</artifactId>
          <version>2.0</version>
      </dependency>

      <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>1.2.17</version>
      </dependency>

      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
        <scope>test</scope>
      </dependency>

      <!-- xml unit for tests -->
      <dependency>
        <groupId>xmlunit</groupId>
        <artifactId>xmlunit</artifactId>
        <version>1.4</version>
        <scope>test</scope>
      </dependency>

    </dependencies>

  </dependencyManagement>

  <build>
    <pluginManagement>
      <plugins>

        <!-- plugin site -->
        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <dependencies>
            <dependency>
              <groupId>org.nuiton.jrst</groupId>
              <artifactId>doxia-module-jrst</artifactId>
              <version>${jrstPluginVersion}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.nuiton.i18n</groupId>
          <artifactId>i18n-maven-plugin</artifactId>
          <version>${nuitonI18nVersion}</version>
          <configuration>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.nuiton.jaxx</groupId>
          <artifactId>jaxx-maven-plugin</artifactId>
          <version>${jaxxVersion}</version>
        </plugin>

        <plugin>
          <groupId>org.nuiton.eugene</groupId>
          <artifactId>eugene-maven-plugin</artifactId>
          <version>${eugenePluginVersion}</version>
        </plugin>

        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>webstart-maven-plugin</artifactId>
          <version>${webstartPluginVersion}</version>
          <configuration>

            <libPath>lib</libPath>
            <makeArchive>false</makeArchive>
            <verbose>false</verbose>
            <canUnsign>false</canUnsign>
            <jnlp>
              <mainClass>${maven.jar.main.class}</mainClass>
              <allPermissions>true</allPermissions>
              <offlineAllowed>true</offlineAllowed>
            </jnlp>
            <sign>
              <keystore>${keystorepath}</keystore>
              <storepass>${keystorepass}</storepass>
              <alias>${keyalias}</alias>
              <keypass>${keypass}</keypass>
              <verify>true</verify>
              <keystoreConfig>
                <delete>false</delete>
                <gen>false</gen>
              </keystoreConfig>
            </sign>
            <gzip>true</gzip>
            <unsign>true</unsign>
            <canUnsign>false</canUnsign>
            <!--Can't use pack2000 since there is some already signed jar-->
            <!--<pack200>true</pack200>-->
            <jnlpExtensions>
              <!--jnlpExtension>
                <name>sun</name>
                <title>Sun MicroSystems</title>
                <vendor>Sun MicroSystems, Inc.</vendor>
                <includes>
                  <include>javax.help:javahelp</include>
                </includes>
              </jnlpExtension-->
              <jnlpExtension>
                <name>jxlayer</name>
                <title>Swing labs JXLayer</title>
                <vendor>Swing Labs</vendor>
                <includes>
                  <include>org.swinglabs:jxlayer</include>
                </includes>
              </jnlpExtension>
            </jnlpExtensions>
          </configuration>
          <dependencies>
            <!-- TODO(pw): This dependency is just a workaround for 1.0-beta-6
                 until 1.0-beta-7 is released. -->
            <dependency>
              <groupId>org.codehaus.mojo</groupId>
              <artifactId>keytool-api-1.7</artifactId>
              <version>1.5</version>
            </dependency>
          </dependencies>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

</project>
